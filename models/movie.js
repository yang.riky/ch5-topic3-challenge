const fs = require("fs");

class Movie {

    constructor() {
        const fs = require("fs");
        this.movies = new Map(Object.entries(JSON.parse(fs.readFileSync("./public/movies.json", "utf-8"))));
        //async
    }

    #getNewId() {
        let newId = 1;

        if (this.movies.size > 0) {
            newId = Math.max(...this.movies.keys()) + 1;
        }

        return newId;
    }

    addMovie(movie) {
        movie.id = this.#getNewId();
        this.movies.set(String(movie.id), movie);
        this.#writeToJson();
    }

    getMovies(id) {
        if (id) {
            return this.movies.get(id);
        } else {
            return this.movies;
        }
    }

    updateMovie(id, movie) {
        this.movies.set(id, movie);
        this.#writeToJson();
    }

    deleteMovie(id) {
        this.movies.delete(id);
        this.#writeToJson();
    }

    #writeToJson() {
        fs.writeFileSync("./public/movies.json", JSON.stringify(
            Object.fromEntries(this.movies)
        ));
    }
}

module.exports = Movie;