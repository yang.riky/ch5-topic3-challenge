const express = require('express');
const router = express.Router();
const Movie = require("../models/movie.js");

const movie = new Movie();

router.get('/add', function (req, res, next) {
  res.render('addEditMovie', {
    title: "Add Movie", "action": "add", "value": {
      "title": "", "genres": [], "duration": 0, "studio": ""
    }
  });
});

router.post('/add', function (req, res, next) {
  movie.addMovie({
    "id": "0", "title": req.body.title, "genres": req.body.genres.split(","),
    "duration": req.body.duration, "studio": req.body.studio
  });
  res.redirect('/');
});

/* GET movies listing. */
router.get('/', function (req, res, next) {
  res.render('movies', { movies: movie.getMovies() });
});

router.get('/:id', function (req, res, next) {
  res.redirect('/movies/edit/' + req.params.id);
});

router.get('/edit/:id', function (req, res, next) {
  value = movie.getMovies(req.params.id);

  res.render('addEditMovie', {
    title: "Edit Movie '" + movie.getMovies(req.params.id).title + "'",
    action: "edit/" + req.params.id,
    "value": value
  });
});

router.post('/edit/:id', function (req, res, next) {
  movie.updateMovie(req.params.id, {
    "id": req.params.id, "title": req.body.title, "genres": req.body.genres.split(","),
    "duration": req.body.duration, "studio": req.body.studio
  });
  res.redirect('/');
});

router.get('/delete/:id', function (req, res, next) {
  movie.deleteMovie(req.params.id);
  res.redirect('/');
});

module.exports = router;
